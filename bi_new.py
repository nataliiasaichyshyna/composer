import airflow

from airflow.contrib.operators import bigquery_operator
from airflow.models import Variable

from new_constants import get_default_args


def rebuild_dimensions_query():
    dim_dataset_tables = [
        'bi_star.dim_Calendar',
        'bi_star.dim_Customer',
        'bi_star.dim_Vendor',
        'bi_star.dim_Employee',
        'bi_star.mv_LocationCustomLookup',
        'bi_star.mv_DefaultSellPrice',
        'bi_star.mv_PrimaryVendor',
        'bi_star.mv_UPC',
        'bi_star.mv_CLU',
        'bi_star.mv_EID',
        'bi_star.mv_BasePrice',
        'bi_star.mv_StyleCustomLookup',
        'bi_star.mv_ItemCustomLookup',
        'bi_star.mv_Price',
        'bi_star.dim_Location',
        'bi_star.dim_Item',
        'bi_star.dim_ExchangeRateToBase'
    ]

    query = ''

    for dset_table in dim_dataset_tables:
        query += """
                    CREATE OR REPLACE TABLE {dataset_table}
                    AS SELECT *
                    FROM `{dataset_table}_view`;
                """.format(dataset_table=dset_table)

    return query


# rebuild partition tables (daily)
def create_rebuild_partitioned_tables_dag(client_dag_id,
                                          client: dict,
                                          schedule_interval: str) -> airflow.DAG:
    recreate_flat_list = [
        {'source': 'bi_star.flat_Sale_view', 'target': 'bi_star.flat_sale'},
        {'source': 'bi_star.flat_Ledger_view', 'target': 'bi_star.flat_Ledger'},
        {'source': 'bi_star.fact_Inventory_view',
         'target': 'bi_star.fact_Inventory'}
    ]

    query = rebuild_dimensions_query()

    query += """
                UPDATE
                    {dataset}.dbo_LedgerDetail ld
                SET
                CostAgg = li.Cost
                FROM (
                SELECT
                    LineInId AS LineId,
                    SUM(CASE
                        WHEN CostType BETWEEN 1 AND 63 THEN cost
                        ELSE 0 END) AS Cost,
                    1 AS LineDirection
                FROM
                    {dataset}.dbo_LedgerCost_view
                GROUP BY
                    LineInId
                UNION ALL
                SELECT
                    LineOutId AS LineId,
                    SUM(CASE
                        WHEN CostType BETWEEN 64 AND 127 THEN Cost
                        ELSE 0 END) AS Cost,
                    -1 AS LineDirection
                FROM
                    {dataset}.dbo_LedgerCost_view
                GROUP BY
                    LineOutId ) li
                WHERE
                ld.Id = li.LineId
                AND ld.LineDirection = li.LineDirection;
            """.format(dataset=client['target_dataset'])

    for part_flat in recreate_flat_list:
        query += """
                    CREATE OR REPLACE TABLE {target}
                    partition by    Date_Part
                    cluster by  
                        RetailYear, 
                        RetailQuarter, 
                        RetailMonth, 
                        RetailWeek
                    as select * 
                    FROM {source};
                """.format(source=part_flat['source'],
                           target=part_flat['target'])

    with airflow.DAG(
            client_dag_id,
            default_args=get_default_args(client['gcp_location']),
            catchup=False,
            schedule_interval=schedule_interval,
            max_active_runs=1,
            concurrency=1) as dag:
        bigquery_operator.BigQueryOperator(
            bigquery_conn_id=client['bq'],
            task_id='rebuild_part',
            sql=query,
            use_legacy_sql=False,
        )
    return dag


# rebuild flat tables (every 20 mins)
def create_rebuild_tables_dag(client_dag_id,
                              client: dict,
                              schedule_interval: str) -> airflow.DAG:
    recreate_flat_list = [
        {'source': 'bi_star.flat_Purchase_view',
         'target': 'bi_star.flat_Purchase', 'select_fields': '*'},
        {'source': 'bi_star.flat_PurchaseOrder_view',
         'target': 'bi_star.flat_PurchaseOrder', 'select_fields': '*'},
        {'source': 'bi_star.flat_SalesOrder_view',
         'target': 'bi_star.flat_SalesOrder', 'select_fields': '*'},
        {'source': 'bi_star.flat_ShipMemo_view',
         'target': 'bi_star.flat_ShipMemo', 'select_fields': '*'},
        {'source': 'bi_star.flat_Adjustment_view',
         'target': 'bi_star.flat_Adjustment', 'select_fields': '*'},
        {'source': 'bi_star.flat_Transfer_view',
         'target': 'bi_star.flat_Transfer', 'select_fields': '*'},
        {'source': 'bi_star.flat_Customer_full_view',
         'target': 'bi_star.flat_Customer',
         'select_fields': '*'}
    ]

    query = ""

    for flat in recreate_flat_list:
        query += """
                    CREATE OR REPLACE TABLE {target}
                    as select {select_fields}
                    FROM {source};
                 """.format(source=flat['source'],
                            target=flat['target'],
                            select_fields=flat['select_fields'])

    with airflow.DAG(
            client_dag_id,
            default_args=get_default_args(client['gcp_location']),
            catchup=False,
            schedule_interval=schedule_interval,
            max_active_runs=1,
            concurrency=2) as dag:
        bigquery_operator.BigQueryOperator(
                bigquery_conn_id=client['bq'],
                task_id='rebuild_tables_query',
                sql=query,
                use_legacy_sql=False,
            )
    return dag


# append ledger, sales and etc.
def create_append_dag(client_dag_id,
                      client: dict,
                      schedule_interval: str) -> airflow.DAG:
    append_flat_list = [
        {'source': 'bi_star.flat_Sale_append', 'target': 'bi_star.flat_sale'},
        {'source': 'bi_star.flat_Ledger_append',
         'target': 'bi_star.flat_Ledger'}
    ]
    with airflow.DAG(
            client_dag_id,
            default_args=get_default_args(client['gcp_location']),
            catchup=False,
            schedule_interval=schedule_interval,
            max_active_runs=1) as dag:
        previous_dependency = None
        for append_flat in append_flat_list:
            op = bigquery_operator.BigQueryOperator(
                bigquery_conn_id=client['bq'],
                task_id=append_flat['source'],
                sql="""
                                INSERT INTO
                                    {target}
                                SELECT
                                    *
                                FROM
                                    {source}
                                """.format(source=append_flat['source'],
                                           target=append_flat['target']),
                use_legacy_sql=False,
            )
            if previous_dependency is not None:
                previous_dependency >> op
            previous_dependency = op
    return dag


def create_dimensions_rebuild_dag(client_dag_id,
                                  client: dict,
                                  schedule_interval: str) -> airflow.DAG:
    query = rebuild_dimensions_query()

    with airflow.DAG(
            client_dag_id,
            default_args=get_default_args(client['gcp_location']),
            catchup=False,
            schedule_interval=schedule_interval,
            max_active_runs=1,
            concurrency=1) as dag:
        bigquery_operator.BigQueryOperator(
            bigquery_conn_id=client['bq'],
            task_id='rebuild_dim',
            sql=query,
            use_legacy_sql=False,
        )
    return dag


client_connections = Variable.get('clients', deserialize_json=True)


for client in client_connections:
    # THEORYOMS-2163 see in customizations.py
    if client['name'] == 'FR_Theory_prd':
        continue

    client_dag_id = "test___{}_rebuild_dimensions".format(client['name'])
    globals()[client_dag_id] = create_dimensions_rebuild_dag(client_dag_id, client, '55 0-4/3,10-24/3 * * *')

    client_dag_id = "test___{}_append_new_data".format(client['name'])
    globals()[client_dag_id] = create_append_dag(client_dag_id, client, '7/20 * * * *')

    client_dag_id = "test___{}_rebuild_partition_tables".format(client['name'])
    globals()[client_dag_id] = create_rebuild_partitioned_tables_dag(client_dag_id, client, '3 5,10 * * *')

    client_dag_id = "test___{}_rebuild_tables".format(client['name'])
    globals()[client_dag_id] = create_rebuild_tables_dag(client_dag_id, client, '*/20 * * * *')