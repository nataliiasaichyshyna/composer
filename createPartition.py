import datetime
import airflow

from airflow.contrib.operators.bigquery_operator import BigQueryOperator

table = 'PendingInventoryMovement'

schedule_interval = '@once'

client = {
    'bq': 'bigquery_twc_bi',
}

default_args = {
    'owner': 'BI',
    'depends_on_past': False,
    'start_date': datetime.datetime(2021, 8, 26),
    'email': [''],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': datetime.timedelta(minutes=5),
}

def create_partition_query(table):

    query = '''
                CREATE OR REPLACE TABLE `twc-bidev.bi_star.rpt_{0}_part`
                PARTITION BY DateAdded
                AS SELECT * FROM `twc-bidev.bi_star.rpt_{0}_v1`;
            '''.format(table)

    return query

with airflow.DAG(
    
    'create_partittion_for_IPM',
    default_args=default_args,
    description='partition_for_PendingInventoryMovement',
    catchup=False,
    schedule_interval=schedule_interval,
    max_active_runs=1,
    concurrency=1
) as dag:

    create_partition = BigQueryOperator(
        task_id='create_partition',
        bigquery_conn_id=client['bq'],
        sql=create_partition_query(table),
        use_legacy_sql=False,
    )
