import datetime
import airflow

from airflow.operators.python_operator import PythonOperator

from airflow.contrib.hooks.redis_hook import RedisHook
from airflow.contrib.hooks.bigquery_hook import BigQueryHook

from airflow.models import Variable


schedule_interval = '@once'

client_connections = Variable.get('clients', deserialize_json=True)

default_args = {
    'owner': 'BI',
    'depends_on_past': False,
    'start_date': datetime.datetime(2021, 9, 2),
    'email': [''],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
}

def get_bq_sql(dataset):

    bq_sql = '''
                SELECT table_name 
                FROM {}.INFORMATION_SCHEMA.TABLES 
                WHERE table_type='BASE TABLE';
            '''.format(dataset)
    
    return bq_sql 


def reset_redis_keys(**kwargs):

    client_name = kwargs['dag_run'].conf.get('name')
    client = next((item for item in client_connections if item['name'] == client_name), None)

    redis = RedisHook().get_conn()
    bq_hook = BigQueryHook(bigquery_conn_id=client['bq'], use_legacy_sql=False)
    bq_conn = bq_hook.get_conn()
    bq_cursor = bq_conn.cursor()
    bq_cursor.execute(get_bq_sql(client['target_dataset']))
    bq_result = bq_cursor.fetchall()
    
    for table in bq_result:

        key = client['name'] + table[0]
        redis.delete(key)

    
with airflow.DAG(
    'ResetKeys',
    default_args = default_args,
    description = 'Reset_redis_keys',
    catchup = False,
    schedule_interval = schedule_interval,
    max_active_runs = 1,
    concurrency = 1
    ) as dag:

    reset_keys_operator = PythonOperator(
        task_id='reset_keys',
        python_callable = reset_redis_keys,
        provide_context=True
    )
