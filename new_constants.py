import datetime

from pendulum import timezone 
from slack_notification import task_fail_slack_notification

tz = {
    'US' : timezone('UTC'),
    'EU' : timezone('EET')
}

default_args = {
    'owner': 'BI',
    'depends_on_past': False,
    'email': [''],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': datetime.timedelta(minutes=1),
    'start_date': datetime.datetime(2020, 1, 1),
    'on_failure_callback': task_fail_slack_notification
}

def get_default_args(timezone):

    default_args['start_date'] = datetime.datetime(2020, 1, 1, tzinfo=tz[timezone])
    return default_args


BQ_AUDIT_CONNECTION = "bigquery_common"

DEFAULT_CRM_ATTR_DATA = {
    'recalc': {
        'source_dataset': 'bi_star',
        'source_table': 'crm_attr_view',
        'dest_dataset': 'bi_star',
        'dest_table': 'crm_attr_current',
        'schedule': '25 10 * * *'
    },
    'history': {
        'source_dataset': 'bi_star',
        'source_table': 'crm_attr_current',
        'dest_dataset': 'bi_star',
        'dest_table': 'crm_attr_history',
        'schedule': '40 10 * * *',
        'condition': 'RecModified > CURRENT_TIMESTAMP()'
    }
}

HARD_DELETE_LIST = 'hard_delete_list'

TSC_ADDRESS = 'https://dub01.online.tableau.com'
TSC_SITE_ID = 'teamworktest'
TSC_SFTP_CREDENTIALS_PATH = '/home/airflow/gcs/dags/configs/credentials.json'
SFTP_ADDRESS = 'sftp.cloudwk.com'