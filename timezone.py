import datetime
import airflow

from pendulum import timezone
from airflow.operators.python_operator import PythonOperator
from airflow.models import Variable


tz = {
    'US': timezone('Europe/Paris'),
    'EU': timezone('Europe/Kiev')
}

client_connections = Variable.get('clients', deserialize_json=True)

default_args = {
    'owner': 'BI',
    'depends_on_past': 'BI',
    'email': [''],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
}

def print_message(**kwargs):

    current_tz = kwargs['tz']
    print('Current timezone', tz[current_tz])


def create_client_dag(client_dag_id,
                      timezone,
                      schedule_interval):

    current_tz = tz[timezone]

    with airflow.DAG(
        client_dag_id,
        default_args = default_args,
        start_date = datetime.datetime(2021, 9, 5, tzinfo=current_tz),
        description = 'timezone test',
        catchup = False,
        schedule_interval = schedule_interval,
        max_active_runs = 1,
        concurrency = 1
    ) as dag:

        PythonOperator(
            task_id = 'print_message',
            python_callable = print_message,
            provide_context=True,
            op_kwargs={'tz': timezone},
        )

    return dag

for client in client_connections:

    client_dag_id = '{}_timezone_test'.format(client['name'])
    globals()[client_dag_id] = create_client_dag(client_dag_id, 
                                                 client['gcp_location'], 
                                                 '*/2 14-15 * * *')
