import datetime
import airflow
import pendulum

from airflow.contrib.operators.mssql_to_gcs import MsSqlToGoogleCloudStorageOperator
from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator

table = 'PendingInventoryMovement'

schedule_interval = '10 */2 * * *'

local_tz = pendulum.timezone("Europe/Kiev")

client = {
    'mssql': 'mssql_qapro',
    'bq': 'bigquery_twc_bi',
}

default_args = {
    'owner': 'BI',
    'depends_on_past': False,
    'start_date': datetime.datetime(2021, 8, 19, tzinfo=local_tz),
    'email': [''],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': datetime.timedelta(minutes=5),
}


def create_query(table):

    query = '''
                SELECT
                    ROW_NUMBER() OVER(ORDER BY RecModified) AS RecId, 
                    CAST(ItemId as NVARCHAR(36)) as ItemId, 
                    ItemPLU,
                    CAST(StyleId as NVARCHAR(36)) as StyleId,
                    StyleNo,
                    CAST(IsNonInventory as int) as IsNonInventory,
                    CAST(LocationId as NVARCHAR(36)) as LocationId,
                    LocationCode,
                    Qty,
                    CostAmount,
                    Category,
                    CAST(IncludedInCommitted as int) as IncludedInCommitted,
                    CAST(IncludedInAvailable as int) as IncludedInAvailable,
                    CAST(IncludedInIncoming as int) as IncludedInIncoming,
                    DocumentType,
                    CAST(DocumentId as NVARCHAR(36)) as DocumentId,
                    DocumentUniversalNo,
                    CAST(DocumentLineId as NVARCHAR(36)) as DocumentLineId,
                    CAST(RecModified as NVARCHAR(100)) as RecModified,
                    CAST(Ignored as int) as Ignored,
                    CAST(SalesOrderId as NVARCHAR(36)) as SalesOrderId,
                    CAST(SalesOrderItemId as NVARCHAR(36)) as SalesOrderItemId,
                    SalesOrderType,
                    SalesOrderTypeLabel,
                    CAST(TransferSourceLocationId as NVARCHAR(36)) as TransferSourceLocationId,
                    CAST(TransferTargetLocationId as NVARCHAR(36)) as TransferTargetLocationId

                FROM [CloudHQ].[rpt].[{0}];

            '''.format(table)

    return query


with airflow.DAG(

    'test_mssql_to_gcs_dag_v_0',
    default_args = default_args,
    description = 'natasha_dag',
    catchup=False,
    schedule_interval = schedule_interval,
    max_active_runs = 1,
    concurrency = 1
    ) as dag:

    move_to_gcs = MsSqlToGoogleCloudStorageOperator(
        task_id='move_inventory_to_gcs',
        sql=create_query(table),
        bucket='europe-west3-bi-bef1223a-bucket',
        filename='data/pending_inventory/export.json',
        schema_filename='schemas/pending_inventory/export.json',
        mssql_conn_id=client['mssql'],
        google_cloud_storage_conn_id=client['bq']
    )

    move_to_bq = GoogleCloudStorageToBigQueryOperator(
        task_id='move_inventory_to_bq',
        bucket='europe-west3-bi-bef1223a-bucket',
        source_objects=['data/pending_inventory/export.json'],
        schema_object='schemas/pending_inventory/export.json',
        source_format='NEWLINE_DELIMITED_JSON',
        write_disposition='WRITE_APPEND',
        destination_project_dataset_table='bi_star.rpt_PendingInventoryMovement_part',
        bigquery_conn_id=client['bq'],
        google_cloud_storage_conn_id=client['bq'],
        autodetect=False,
        time_partitioning={'type':'DAY'}
    )

    move_to_gcs >> move_to_bq